FROM registry.gitlab.com/django-startdemy/startdemy-app:latest

ENV ENVIRONMENT=production

RUN set -eux; \
    gosu app python3 -m venv /python; \
    gosu app /python/bin/pip install -U pip setuptools wheel

COPY --chown=app:app requeriments.txt /requeriments.txt

RUN set -eux; \
    gosu app pip install --no-cache-dir -r /requeriments.txt;

COPY --chown=app:app . /app

RUN set -eux; \
    export DJANGO_SETTINGS_MODULE=ashe.settings.build; \
    \
    gosu app pip install --no-cache-dir -e . ; \
    \
    gosu app mkdir -p public/media;

EXPOSE 8000

CMD ["/app/bin/runserver"]
