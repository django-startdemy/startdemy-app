CURRENT_DIR=$(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))

.PHONY: default
default: install

.PHONY: install
install:
	pip install -r requeriments.txt
	pip install -r requirements-develop.txt
	pip install -e $(PWD)
	pip install requests

.PHONY: runserver
runserver:
	python src/manage.py runserver 0.0.0.0:8000

.PHONY: backend
backend:
	docker-compose run --rm --service-ports backend --shell

.PHONY: collectstatic
collectstatic: check
	rm -rf public/static
	rm -rf public/index.html
	env DJANGO_SETTINGS_MODULE=ashe.settings.build \
		python src/manage.py \
			collectstatic \
			--verbosity 0 \
			--noinput \
			--clear

check:
	# Check you are running inside the container
	test -d /app

.PHONY: deploy
deploy:
	heroku container:push web -a startdemy
	heroku container:release web -a startdemy
	heroku run bash -c "python src/manage.py migrate"