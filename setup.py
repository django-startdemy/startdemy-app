from setuptools import setup, find_packages

setup(
    name='appstartdemy.com',
    version='1.0',
    author='josuedjh',
    author_email='hola@gmail.com',
    package_dir={'': 'src'},
    packages=find_packages('src'),
    zip_safe=False,
    include_package_data=True,
)
