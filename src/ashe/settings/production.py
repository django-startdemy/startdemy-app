from .base import *  # noqa

DEBUG = False

DATABASES = {
    "default": dj_database_url.parse(
        url=os.environ.get(
            "DATABASE_URL", "postgres://postgres@database/master_rochester"
        ),
        engine="django.db.backends.postgresql_psycopg2",
        conn_max_age=600,
        ssl_require=True,
    )
}