from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.generic import TemplateView


urlpatterns = [
    path("admin/", admin.site.urls),
    path("", TemplateView.as_view(template_name="landing.html")),

]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.views import serve as static_serve

    staticpatterns = static(settings.STATIC_URL, view=static_serve)
    mediapatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns = staticpatterns + mediapatterns + urlpatterns

    if "debug_toolbar" in settings.INSTALLED_APPS:
        debugpatterns = [url(r"^__debug__/", include("debug_toolbar.toolbar"))]
        urlpatterns = debugpatterns + urlpatterns
